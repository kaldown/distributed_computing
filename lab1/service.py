# -*- codingL utf-8 -*-

from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import Factory
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor
from dc_db import show_cities

class Service(LineReceiver):
    
    commands = {'city': show_cities}

    def lineReceived(self, line):
        self.make_response(line)

    def make_response(self, request):
        if request in self.commands:
            response = self.commands[request]()
            self.sendLine('{0}\t{1}'.format(request, response))
        else:
            self.sendLine('There is no such command')


class Runner(Factory):
        protocol =  Service

def run_service():
    endpoint = TCP4ServerEndpoint(reactor, 8007)
    endpoint.listen(Runner())
    reactor.run()

if __name__ == '__main__':
    run_service()
