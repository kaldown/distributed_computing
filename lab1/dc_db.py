#!/usr/bin/python2.7

# -*- codingL utf-8 -*-

import psycopg2

def show_cities():
    with psycopg2.connect('dbname=dc') as conn:
        with conn.cursor() as cur:

            cur.execute(
                    """
                    SELECT *
                    FROM city;
                    """
                    )

            return cur.fetchone()

