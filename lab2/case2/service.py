from twisted.protocols.basic import LineReceiver
from twisted.internet.protocol import ServerFactory
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet import reactor

class ChatServer(LineReceiver):
    def __init__(self, users):
        self.authenticated = False
        self.users = users
        self.name = None

    def connectionMade(self):
        self.sendLine('connection estabilished') 
        self.sendLine('What is your name?') 

    def connectionLost(self, reason):
        if self.name in self.users:
            del self.users[self.name]

    def lineReceived(self, line):
        if not self.authenticated:
            self.is_logged(line)
        else:
            self.chat(line)

    def is_logged(self, user):
        if user in self.users:
            self.sendLine('%s already logged in' % user)
        else:
            self.authentication(user)
        
    def authentication(self, user):
        if user == 'kaldown' or user == 'root' or user == 'test':
            self.authenticated = True
            self.name = user
            self.users[self.name] = self
            self.sendLine('Successfully authenticated as %s' % self.name)
            self.sendLine('AUTH_SUCCESS')
        else:
            self.sendLine('Wrong username')
            self.sendLine('What is your name, again?') 

    def chat(self, line):
        if line.split()[0].startswith('@'):
            self.direct_message(line)
        else:
            self.msg_everyone_except_me(line)

    def direct_message(self, line):
        direct_user = line.split()[0][1:]
        direct_message = ' '.join(line.split()[1:])
        if direct_user in self.users:
            self.users[direct_user].sendLine('[{0}]: {1}'.format(self.name, 
                                                                        direct_message))
        else:
            self.sendLine('{0} not logged in'.format(direct_user))

    def msg_everyone_except_me(self, line):
        for user, protocol in self.users.iteritems():
            #if protocol != self:
            protocol.sendLine('<{0}>: {1}'.format(self.name, line))
        

class RunServer(ServerFactory):
    def __init__(self):
        self.users = {}

    def buildProtocol(self, addr):
        return ChatServer(self.users)

if __name__ == '__main__':
    endpoint = TCP4ServerEndpoint(reactor, 8888)
    endpoint.listen(RunServer())
    reactor.run()

