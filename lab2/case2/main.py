from kivy.support import install_twisted_reactor
install_twisted_reactor()

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition

# import twisted reactor *only after* installing wxreactor
from twisted.internet import reactor 
from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineReceiver

class DataForwardingProtocol(LineReceiver):
    def connectionMade(self):
        gui = self.factory.gui
        gui.root.current = 'AuthScreen'
        
    def dataReceived(self, data):
        gui = self.factory.gui

        if 'AUTH_SUCCESS' in data:
            gui.auth = True
            gui.root.current = 'ChatScreen'

        gui.protocol = self
        if gui:
            gui.print_msg(data)

class ChatFactory(ClientFactory):
    def __init__(self, gui):
        self.gui = gui
        self.protocol = DataForwardingProtocol

    def clientConnectionLost(self, transport, reason):
        reactor.stop()

    def clientConnectionFailed(self, transport, reason):
        self.gui.root.current_screen.ids.label.text = "Can't connect"

class SM(ScreenManager):
    transition=FadeTransition()
    

class FindService(Screen):
    pass

class AuthScreen(Screen):
    pass

class ChatScreen(Screen):
    pass

builder = Builder.load_string("""
SM:
    FindService:
    AuthScreen:
    ChatScreen:

<FindService>:
    name: "FindService"
    BoxLayout:
        orientation: 'vertical'
        Label: 
            id: label
        TextInput:
            id: textbox
            size_hint_y: .1
            multiline: False
            on_text_validate: app.connect_to_service(textbox.text)

<AuthScreen>:
    name: "AuthScreen"
    BoxLayout:
        orientation: 'vertical'
        Label: 
            id: label
        TextInput:
            id: textbox
            size_hint_y: .1
            multiline: False
            on_text_validate: app.send_msg(textbox.text)

<ChatScreen>:
    name: "ChatScreen"
    BoxLayout:
        orientation: 'vertical'
        Label: 
            id: label
        TextInput:
            size_hint_y: .1
            id: textbox
            multiline: False
            on_text_validate: app.send_msg(textbox.text)
        """)

class ChatApp(App):
    protocol = None
    auth = False

    def build(self):
        return builder

    def connect_to_service(self, address):
        reactor.connectTCP(str(address), 8888, ChatFactory(self))

    def send_msg(self, msg):
        self.protocol.sendLine(str(msg))
        self.root.current_screen.ids.textbox.text = ''

    def print_msg(self, data):
        if self.auth:
            self.root.current_screen.ids.label.text += str(data)
        else:
            self.root.current_screen.ids.label.text = str(data)

if __name__ == '__main__':
    ChatApp().run()

