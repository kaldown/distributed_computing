import wx
from twisted.internet import wxreactor
wxreactor.install()

# import twisted reactor *only after* installing wxreactor
from twisted.internet import reactor 
from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineReceiver

class ChatFrame(wx.Frame):
    def __init__(self):
        wx.Frame.__init__(self, parent=None, title="Chat")
        self.protocol = None  # twisted Protocol

        sizer = wx.BoxSizer(wx.VERTICAL)
        self.text = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.TE_READONLY)
        self.ctrl = wx.TextCtrl(self, style=wx.TE_PROCESS_ENTER, size=(300, 25))

        sizer.Add(self.text, 5, wx.EXPAND)
        sizer.Add(self.ctrl, 0, wx.EXPAND)
        self.SetSizer(sizer)
        self.ctrl.Bind(wx.EVT_TEXT_ENTER, self.send)

    def send(self, evt):
        self.protocol.sendLine(str(self.ctrl.GetValue()))
        self.ctrl.SetValue("")

class DataForwardingProtocol(LineReceiver):
    def dataReceived(self, data):
        gui = self.factory.gui

        gui.protocol = self
        if gui:
            val = gui.text.GetValue()
            gui.text.SetValue(val + data)
            gui.text.SetInsertionPointEnd()

class ChatFactory(ClientFactory):
    def __init__(self, gui):
        self.gui = gui
        self.protocol = DataForwardingProtocol

    def clientConnectionLost(self, transport, reason):
        reactor.stop()

    def clientConnectionFailed(self, transport, reason):
        reactor.stop()

if __name__ == '__main__':
    app = wx.App(False)
    frame = ChatFrame()
    frame.Show()
    reactor.registerWxApp(app)
    reactor.connectTCP("localhost", 8888, ChatFactory(frame))
    reactor.run()

